﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSSFSDemo3
{
    class Program
    {
        static void Main(string[] args)
        {
            var myDoc = new Document();
            myDoc.Print();
            myDoc.countCharacters();

            Printable printableItem = myDoc;
            printableItem.Print();

            Document theDoc = printableItem as Document;

            if (theDoc != null)
                theDoc.countCharacters();

            string contents = theDoc.Read();
            Console.WriteLine("Contents: " + contents);
        }
    }
}
