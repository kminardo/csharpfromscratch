﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSSFSDemo6
{
    class Program
    {
        static void Main(string[] args)
        {
            var primes = new List<int> { 1, 2, 3, 5, 7, 11, 13, 17, 19, 23 };

            var query = from val in primes
                        where val < 13
                        select val;

            foreach (var val in query)
            {
                Console.WriteLine(val);
            }

            //***********************************************
            var query2 = primes.Where(x => x < 13);

            foreach (var val in query2)
            {
                Console.WriteLine(val);
            }

            //***********************************************
            var query3 = from method in typeof(double).GetMethods()
                         orderby method.Name
                         group method by method.Name into groups
                         select new { MethodName = groups.Key, NumberOfOverloads = groups.Count() };

            foreach (var item in query3)
            {
                Console.WriteLine(item);
            }

            var listOne = Enumerable.Empty<int>();
            var listTwo = Enumerable.Range(1, 20);

            bool listOneEmpty = listOne.Any();
            bool listTwoEmpty = listTwo.Any();

            Console.WriteLine("List One has members? " + listOneEmpty + " list two has members? " + listTwoEmpty);

            Console.WriteLine("ListTwo Has 12? " + listTwo.Contains(12) + " list two has 30? " + listTwo.Contains(30));

            //***********************************************
            var bigList = Enumerable.Range(1, 20);
            var littleList = bigList.Take(5).Select(x => x * 10);

            foreach (var item in littleList)
            {
                Console.WriteLine(item);
            }

            //***********************************************
            string[] postalCodes = { "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL" };
            string[] states = { "Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Deleware", "Florida" };

            var statesWithCodes = postalCodes.Zip(states, (code, state) =>
                code + ": " + state);

            foreach (var state in statesWithCodes)
            {
                Console.WriteLine(state);
            }
        }
    }
}
