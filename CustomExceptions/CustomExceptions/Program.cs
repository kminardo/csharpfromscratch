﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomExceptions
{
    class Program
    {
        public class Tester
        {
            public double DoDivide (double x, double y)
            {
                if (y == 0)
                    throw new DivideByZeroException();
                if (x == 0)
                    throw new MyCustomException("Dividend can't be zero!");
                return x / y;
            }
        }
        public class MyCustomException : Exception
        {
            public MyCustomException(string message) :
                base(message)
            {
            }
        }
        static void Main(string[] args)
        {
            try
            {
                Tester t = new Tester();
                double answer = t.DoDivide(12, 4);
                Console.WriteLine(answer);
                answer = t.DoDivide(0, 4);
                Console.WriteLine(answer);
            }
            catch (DivideByZeroException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (MyCustomException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
