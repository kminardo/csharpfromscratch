Most of the projects through PluralSight's C# From Scratch video series.

If I feel I am familiar with a concept being discussed I will not be submitting the code for it.