﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes2
{
    class Employee
    {
        public int Age { get; set; }
        public string Name { get; set; }
        public double Salary { get; set; }
        public DateTime StartingDate { get; set; }
        public string PhoneNumber { get; set; }

        public void Bonus(double bonusPercent)
        {
            Salary += Salary * bonusPercent;
        }

        public Employee(int age, string name, double salary, DateTime startingDate, string phoneNumber)
        {
            Age = age;
            Name = name;
            Salary = salary;
            StartingDate = startingDate;
            PhoneNumber = phoneNumber;
        }

        public Employee()
        {
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Employee Ken = new Employee()
            {
                Age = 23,
                Name = "Kenneth Minardo",
                Salary = 50000.00,
                StartingDate = new DateTime(2012, 7, 10),
                PhoneNumber = "216-999-1234"
            };

            Console.WriteLine("{0}'s age is {1} he started on {2} and makes {3}",
                Ken.Name, Ken.Age, Ken.StartingDate.ToShortDateString(), Ken.Salary);

            Ken.Bonus(.05);

            Console.WriteLine("{0}'s age is {1} he started on {2} and makes {3}",
                Ken.Name, Ken.Age, Ken.StartingDate.ToShortDateString(), Ken.Salary);

            Employee Mary = new Employee(25, "Mary Jones", 60000, new DateTime(2010, 2, 28),"440-999-1234");

            Console.WriteLine("{0}'s age is {1} he started on {2} and makes {3}",
                Mary.Name, Mary.Age, Mary.StartingDate.ToShortDateString(), Mary.Salary);
        }
    }
}
