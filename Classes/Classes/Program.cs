﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    class Employee
    {
        //private int age;
        //public int Age
        //{
        //    get { return age; }
        //    set { age = value; }
        //}

        //private string name;
        //public int Name
        //{
        //    get { return name; }
        //    set { name = value; }
        //}

        public int Age { get; set; }
        public string Name { get; set; }


    }

    class Program
    {
        static void Main(string[] args)
        {
            Employee Ken = new Employee();
            Ken.Age = 23;
            Console.WriteLine("Ken's age is {0}", Ken.Age);
        }
    }
}
