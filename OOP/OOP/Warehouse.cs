﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP
{
    class Warehouse
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public Warehouse(string Name, int ID)
        {
            Name = this.Name;
            ID = this.ID;
        }

        public Item FindAndReturnItem(int itemId)
        {
            Item returnItem = new Item()
            {
                ID = itemId,
                Name = "Microsoft Office"
            };

            return returnItem;
        }
    }
}
